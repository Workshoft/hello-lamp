<?php include "templates/header.php"; ?>

<h2>Find user based on location</h2>

<form method="post" class="row justify-content-center ">
	<label for="location">Location</label>
	<input type="text" id="location" name="location">
	<input type="submit" name="submit" value="View Results">
</form>

<a href="index.php">Back to home</a>

<?php include "templates/footer.php"; ?>